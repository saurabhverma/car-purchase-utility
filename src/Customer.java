import java.util.*;

public class Customer {

	int custId;
	String name;
	ArrayList<Car> carList = new ArrayList<Car>();

	public Customer() {
	}

	public Customer(int custId, String name) {
		this.custId = custId;
		this.name = name;
	}

	public Customer(Customer c) {
		this.custId = c.custId;
		this.name = c.name;
	}

	public int getId() {
		return custId;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Customer ID : " + custId + "\nCustomer Name : " + name
				+ " \nCar Details : " + carList;
	}

}
