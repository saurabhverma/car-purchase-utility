import java.io.*;
import java.util.*;

public class Controller implements UserChoice {
	static Scanner scannerObj = new Scanner(System.in);
	static BufferedReader br = new BufferedReader(new InputStreamReader(
			System.in));

	public static void main(String[] args) throws Exception {
		int choice;
		String response;
		ArrayList<Customer> customerList = new ArrayList<Customer>();
		do {
			System.out
					.println("\nCar Purchase Utility\n====================\n1. Add new Customer\n2. Add new Car to an existing Customer\n3. List all Customers with their cars sorted by name\n4. List individual Customer based on ID\n5. Generate prizes for customer\n6. Exit\nEnter your choice : ");
			choice = scannerObj.nextInt();
			switch (choice) {
			case ADD_CUSTOMER:
				do {
					addCustomer(customerList);
					System.out
							.println("Do you want to add more customers(y/n)???");
					response = (String) br.readLine();
				} while (response.equals("y") || response.equals("Y"));
				break;
			case ADD_NEW_CAR:
				do {
					addCar(customerList);
					System.out.println("Do you want to add more cars(y/n)???");
					response = (String) br.readLine();
				} while (response.equals("y") || response.equals("Y"));
				break;
			case LIST_ALL_CUSTOMER:
				sortName(customerList);
				break;
			case LIST_INDIVIDUAL_CUST:
				displayWithCustomerId(customerList);
				break;
			case PRIZE:
				givePrize(customerList);
				break;
			case EXIT:
				System.out.println("THANYOU!!!!");
				System.exit(0);
				break;
			default:
				System.out.println("Invalid Entry!");
			}
		} while (true);
	}

	private static void addCustomer(ArrayList<Customer> customerList)
			throws Exception {
		Customer customer = new Customer();
		boolean flag = false;
		System.out.println("Enter customer Id : ");
		int id = customer.custId = scannerObj.nextInt();
		for (Customer cust : customerList) {
			if (id == cust.getId()) {
				flag = true;
			}
		}
		if (flag == true) {
			System.out.println("Customer Id Already Exits!!!!\n");
		} else {
			System.out.println("Enter customer Name : ");
			customer.name = (String) br.readLine();
			customerList.add(customer);
		}
	}

	private static Comparator<Customer> byName() {
		return new Comparator<Customer>() {
			public int compare(Customer o1, Customer o2) {
				return o1.name.compareTo(o2.name);
			}
		};
	}

	public static void sortName(ArrayList<Customer> customerList) {
		Collections.sort(customerList, byName());
		for (int i = 0; i < customerList.size(); i++) {
			Customer customer = customerList.get(i);
			System.out.println(customer);
		}
	}

	public static void displayWithCustomerId(ArrayList<Customer> customerList) {
		System.out.println("Enter Customer Id  ");
		int custId = scannerObj.nextInt();
		for (Customer customer : customerList) {
			if (custId == customer.getId()) {
				System.out.println(customer);
			}
		}
	}

	private static void addCar(ArrayList<Customer> customerList) {
		try {
			boolean flag = false;
			System.out.println("Enter Customer Id  ");
			int custId = scannerObj.nextInt();
			Customer customer = new Customer();
			for (Customer cust : customerList) {
				if (custId == cust.getId()) {
					flag = true;
					customer = cust;
				}
			}
			if (flag == false) {
				System.out.println("Customer Id Doesn't Exits!!!!\n");
			} else {
				System.out
						.println("Enter Car Type : \n1. Toyota\n2. Maruti\n3. Hyundai");
				int choice = scannerObj.nextInt();
				System.out.println("Enter Car Id : ");
				int carId = scannerObj.nextInt();
				System.out.println("Enter Car Model : ");
				String model = (String) br.readLine();
				System.out.println("Enter price : ");
				double price = scannerObj.nextDouble();
				switch (choice) {
				case TOYOTA:
					Toyota toyota = new Toyota(carId, model, price);
					customer.carList.add(toyota);
					break;
				case MARUTI:
					Maruti maruti = new Maruti(carId, model, price);
					customer.carList.add(maruti);
					break;
				case HYUNDAI:
					Hyundai hyundai = new Hyundai(carId, model, price);
					customer.carList.add(hyundai);
					break;
				default:
					System.out.println("Invalid Choice!!!!!!");
					break;
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	static void givePrize(ArrayList<Customer> customerList) {
		int random = 0;
		int[] userKeys = new int[3];
		Random randomGenerator = new Random();
		Set<Integer> selectedList = new HashSet<Integer>();
		Set<Integer> finalSelectedList = new HashSet<Integer>();
		while (random < 6) {
			Customer cust = new Customer(customerList.get(randomGenerator
					.nextInt(customerList.size())));
			selectedList.add(cust.getId());
			random = selectedList.size();
		}

		System.out.print("Enter three Customer IDs :");
		for (int i = 0; i < 3; i++) {
			System.out.print("Enter Customer ID  " + (i + 1) + " : ");
			userKeys[i] = scannerObj.nextInt();
		}
		System.out.println("\nLucky winner : ");

		for (int i = 0; i < 3; i++) {
			if (selectedList.contains(userKeys[i])) {
				finalSelectedList.add(userKeys[i]);
			}
		}
		System.out.println(finalSelectedList);
	}
}
