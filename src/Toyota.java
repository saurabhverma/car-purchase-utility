public class Toyota extends Car {

	Toyota(int carId, String model, double price) {
		super(carId, model, price);
		resale = price * 0.4;
	}
}
