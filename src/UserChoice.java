
public interface UserChoice {
	int ADD_CUSTOMER = 1;
	int ADD_NEW_CAR = 2;
	int LIST_ALL_CUSTOMER = 3;
	int LIST_INDIVIDUAL_CUST = 4;
	int PRIZE = 5;
	int EXIT = 6;
	int TOYOTA = 1;
	int MARUTI = 2;
	int HYUNDAI = 3;
}
