public class Maruti extends Car {

	Maruti(int carId, String model, double price) {
		super(carId, model, price);
		resale = price * 0.6;
	}

}
