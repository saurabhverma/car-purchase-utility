public class Hyundai extends Car {
	Hyundai(int carId, String model, double price) {
		super(carId, model, price);
		this.resale = price * 0.8;
	}

}
