
public class Car {
	int carId;
	String model;
	double price;
	double resale;

	public Car(){}
	public Car(int carId, String model, double price){
		this.carId=carId;
		this.model=model;
		this.price=price;
	}
	
	@Override
	public String toString() {
		return "\nCar ID : " + carId + "\nCar Model : " + model + "\nCar Price : " + price + "\nResale Value : "+resale;
	}
}
